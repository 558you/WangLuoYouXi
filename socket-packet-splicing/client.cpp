#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define BUF_SIZE 100

int main(int argc, const char * argv[]) {
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    
    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_port = htons(1234);
    connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
    
    
    char buf_send[BUF_SIZE] = {0};
    printf("Input a string:");
    gets(buf_send);
    printf("send-----0\n");
    for (int i = 0; i < 3; i++) {
        int size = write(sock, buf_send, strlen(buf_send));
        printf("send--size=%d---%d\n",size, i+1);
    }
    
    char bufRecv[BUFSIZ] = {0};
    int size = read(sock, bufRecv, BUFSIZ);
    if (size == 0) {
        close(sock);
        return  0 ;
    }
    
    printf("read---size=%d\n", size);
    printf("Message form server:%s\n", bufRecv);
    
    close(sock);
    
    return 0;
}
