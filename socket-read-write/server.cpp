#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUF_SIZE 100

int main(int argc, const char * argv[]) {
    
    char *filename = "文件目录";
    FILE *fp = fopen(filename, "rb");
    if (fp == nullptr) {
        printf("Cannot open file, press any key to exit!\n");
        exit(0);
    }
    
    int serv_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    
    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_port = htons(1234);
    bind(serv_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
    
    listen(serv_sock, 20);
    
    struct sockaddr_in clnt_addr;
    socklen_t clnt_addr_size = sizeof(clnt_addr);
    int clnt_sock = accept(serv_sock, (struct sockaddr*)&clnt_addr, &clnt_addr_size);
    
    char buffer[BUF_SIZE] = {0};
    int strlen;
    
    while ((strlen = fread(buffer, 1, BUF_SIZE, fp)) > 0) {
        write(clnt_sock, buffer, strlen);
        printf("发送中。。。。\n");
    }
    
    shutdown(clnt_sock, SHUT_RD);
    printf("关闭输入流。。。。\n");
    read(clnt_sock, buffer, BUF_SIZE);
    
    fclose(fp);
    close(clnt_sock);
    close(serv_sock);
 
    return 0;
}
