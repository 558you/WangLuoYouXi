#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define BUFSIZE 100

int main(int argc, const char * argv[]) {

    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_port = htons(1234);
    
    
    
    char buf_send[BUFSIZE] = {0};
    char buf_recv[BUFSIZE] = {0};

    while(1) {
        int sock = socket(AF_INET, SOCK_STREAM, 0);
        connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
        printf("Input a string:");
        scanf("%s", buf_send);
        write(sock, buf_send, strlen(buf_send));
        read(sock, buf_recv, BUFSIZE);
        printf("Message form server:%s\n", buf_recv);

        memset(&buf_send, 0, BUFSIZE);
        memset(&buf_recv, 0, BUFSIZE);

        close(sock);
    }
    
    return 0;
}