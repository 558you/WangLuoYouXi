
#ifndef ClientSocket_hpp
#define ClientSocket_hpp

#include "TcpSocket.h"
#include <string>

class ClientSocket:public Socket
{
public:
    ClientSocket(const std::string& host, const int port);
    virtual ~ClientSocket();
    
    bool Send(const std::string& message);
    int Receive(std::string& message);
};

#endif /* ClientSocket_hpp */
