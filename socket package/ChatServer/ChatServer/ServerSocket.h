#ifndef ServerSocket_hpp
#define ServerSocket_hpp

#include "TcpSocket.h"

class ServerSocket:public Socket
{
public:
    ServerSocket(const int port);
    ServerSocket();
    virtual ~ServerSocket();
    
    void Accept(Socket& socket);
};


#endif /* ServerSocket_hpp */
