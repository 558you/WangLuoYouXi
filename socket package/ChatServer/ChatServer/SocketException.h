
#ifndef SocketException_hpp
#define SocketException_hpp

#include <string>

class SocketException
{
public:
    SocketException(std::string description) {};
    ~SocketException() {};
    
    std::string Description() { return m_description; }
private:
    std::string m_description;
};


#endif /* SocketException_hpp */
